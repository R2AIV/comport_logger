#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>

int main(void)
{
	int comport_fd;
	uint8_t InputBuffer[128] = {0};
	uint8_t PrintBuffer[128] = {0};
	uint16_t bytes = 0;

	printf("Comport logger. Press CTRL-C to quit...\r\n");
	printf("See output in comport.log\r\n");
	

	comport_fd = open("/dev/ttyUSB0", O_RDONLY);

	while(1)
	{
		memset((void *)&InputBuffer,0,sizeof(InputBuffer));
		memset((void *)&PrintBuffer,0,sizeof(PrintBuffer));
		bytes=read(comport_fd, (void *)&InputBuffer[0],sizeof(InputBuffer));
		printf("Read %d bytes...\r\n", bytes);
		strcpy((void *)&PrintBuffer, (void *)&InputBuffer);
		printf("Read data: \r\n%s\r\n\r\n", PrintBuffer);
	};

	close(comport_fd);
};
